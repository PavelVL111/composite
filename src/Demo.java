public class Demo {
    public static void main(String[] args) {
        System.out.println(toWrapUp().getPrice());
    }

    public static Parcel toWrapUp() {
        Box generalBox = new Box();
        Box subBox1 = new Box();
        Box subBox2 = new Box();
        Box subBox3 = new Box();

        Product product1 = new Product(1);
        Product product2 = new Product(2);

        Product product3 = new Product(4);
        Product product4 = new Product(8);

        Product product5 = new Product(2);

        subBox1.addParcel(product1);
        subBox1.addParcel(product2);

        subBox2.addParcel(product3);
        subBox2.addParcel(product4);

        generalBox.addParcel(subBox1);
        generalBox.addParcel(subBox2);
        generalBox.addParcel(subBox3);

        generalBox.addParcel(product5);

        return generalBox;
    }
}
