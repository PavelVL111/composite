import java.util.ArrayList;
import java.util.List;

public class Box implements Parcel {

    private List<Parcel> parcelList = new ArrayList<>();

    public void addParcel(Parcel parcel) {
        this.parcelList.add(parcel);
    }

    @Override
    public double getPrice() {
        if (parcelList.size() == 0) {
            return 0;
        } else {
            double totalPriceOfBox = 0;
            for (Parcel parcel : this.parcelList) {
                totalPriceOfBox += parcel.getPrice();
            }
            return totalPriceOfBox;
        }
    }
}
